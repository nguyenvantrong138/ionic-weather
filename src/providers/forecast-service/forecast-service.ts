import { Injectable, EventEmitter } from '@angular/core';
import { Jsonp,Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { FORECAST_CONFIG } from '../constants/constants';
import { MyLocation,Forecast } from '../model/model';
import { Observable } from "rxjs/Observable";
import { DatabaseServiceProvider } from "../database-service/database-service";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
/*
  Generated class for the ForecastServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ForecastService {

  constructor(public http: Http, public databaseService : DatabaseServiceProvider ,public jsonp : Jsonp) {
    console.log('Hello ForecastServiceProvider Provider');
  }
  getForecast(location : MyLocation) : Observable<Forecast> {
    let emitterForecast: EventEmitter<Forecast> = new EventEmitter<Forecast>();
    // this.databaseService.getForecast(location.name)
    // .then(data => console.log("data",data))
    // .catch(data => {
      this.getServerData(location, emitterForecast);
    // });
      
    return emitterForecast;
  }
  private getServerData(location: MyLocation, emitterForecast: EventEmitter<Forecast>){
    this.jsonp.get(this.getRequestUri(location))
      .map(res => res.json())
      .catch(Error => Error)
      .subscribe(data => {
        console.log("data day roi",data);
        emitterForecast.emit(data);
        // self.databaseService.addForecast(location.name, data);
      });
  }
  private getRequestUri(location: MyLocation): string {
  return FORECAST_CONFIG.API_ENDPOINT + FORECAST_CONFIG.API_KEY + '/' + location.lat + ',' + location.lng
    + '?units=us&lang=en&exclude=currently,minutely,alerts,flags&callback=JSONP_CALLBACK';
  }
}
