import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { MetricDistance, MetricLength, MetricPressure, MetricTemp } from '../model/model';
import 'rxjs/add/operator/map';

/*
  Generated class for the ConstantsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ConstantsProvider {

  constructor(public http: Http) {
    console.log('Hello ConstantsProvider Provider');
  }
}
export const CONFIG = {
  METRICS: 'metrics',
  HOME_LOCATION: 'homeLocation'
};
export const FORECAST_CONFIG = {
  API_ENDPOINT: 'https://api.darksky.net/forecast/',
  API_KEY: 'b1fb478b5a9cabf65a99be3a31ec93f2'
};

export const DEFAULT_METRICS = {
  temp: MetricTemp.F,
  length: MetricLength.IN,
  distance: MetricDistance.MI,
  time: 12,
  pressure: MetricPressure.MBAR
};