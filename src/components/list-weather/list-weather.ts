import { Component, Input, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Subscription } from "rxjs/Subscription";
import { MyLocation,Forecast } from "../../providers/model/model";
import { ForecastService, UtilServiceProvider, DataPoint, Metrics, DEFAULT_METRICS } from "../../providers/index";
import * as _ from 'lodash';
// import { MyLocation } from '../../providers/'
/**
 * Generated class for the WeatherListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'list-weather',
  templateUrl: 'list-weather.html',
})
export class ListWeatherComponent {
  @Input() location: MyLocation;
  forecastSubscriber : Subscription;
  metrics: Metrics;
  locationObj : MyLocation = new MyLocation();
  todayForecast : DataPoint;
  forecast : Forecast;
  hourlyArray: Array<{
    time: number,
    icon: string,
    temperature: number
  }> = [];
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public forecastService : ForecastService,
     public utilService : UtilServiceProvider) {
  }

  ngOnChanges(){
    this.ngOnInit1();
  }
  ngOnInit1(){
    console.log("ngOnInit",this.location);
    if(this.location){
      this.getForecast(this.location);
      this.metrics = JSON.parse(localStorage.getItem("metric")) || DEFAULT_METRICS;
    }
  }

    getForecast(location: MyLocation) {
    this.forecastSubscriber = this.forecastService.getForecast(location)
      .subscribe((data: Forecast) => {
        this.forecast = data;
        console.log("forecast",this.forecast);
        if (this.forecast && this.forecast.daily && this.forecast.daily.data) {
          this.todayForecast = this.forecast.daily.data[0];
        }
        this.hourlyArray = [];
        let currentHour = this.utilService.getCurrentHour(this.forecast.timezone);
        console.log("currenhour",currentHour);
        let flag = false;
        _.forEach(this.forecast.hourly.data, (obj: DataPoint) => {
          if (!flag && this.utilService.epochToHour(obj.time, this.forecast.timezone) < currentHour) {
            return;
          }
          flag = true;
          this.hourlyArray.push({
            time: obj.time,
            icon: obj.icon,
            temperature: obj.temperature
          });
          if (this.hourlyArray.length > 10) {
            return false;
          }
        });
      }, err => {
        console.error(err);
      });
  }

  itemClicked(page){
    this.navCtrl.push('WeatherDetailPage',{
      forecast: this.forecast,
      currentForecast: page,
      currentLocation: this.location,
      metrics: this.metrics
    });

  }
}
