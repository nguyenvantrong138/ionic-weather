import { NgModule } from '@angular/core';
import { ListWeatherComponent } from "./list-weather/list-weather";
import { IonicApp, IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [ListWeatherComponent],
	imports: [IonicModule],
	exports: [ListWeatherComponent],
	bootstrap: [IonicApp],
})
export class ComponentsModule {}
