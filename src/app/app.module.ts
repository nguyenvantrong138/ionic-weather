import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule,JsonpModule } from '@angular/http';
import { WorldPage } from '../pages/world/world';
import { TabsPageModule } from '../pages/tabs/tabs.module';
import { IonicStorageModule } from '@ionic/storage';
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ComponentsModule } from '../components/components.module';
import { ConstantsProvider } from '../providers/constants/constants';
import { DatabaseServiceProvider } from '../providers/database-service/database-service';
import { ForecastService } from '../providers/forecast-service/forecast-service';
import { SqlProvider } from '../providers/sql/sql';
import { UtilServiceProvider } from '../providers/util-service/util-service';
import { PipesModule } from "../pipes/pipes.module";
import { HomePage } from "../pages/weather-home/weather-home";
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    WorldPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    PipesModule,
    JsonpModule,
    TabsPageModule,
    ComponentsModule,
    IonicStorageModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    WorldPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConstantsProvider,
    DatabaseServiceProvider,
    ForecastService,
    SqlProvider,
    UtilServiceProvider
  ]
})
export class AppModule {}
