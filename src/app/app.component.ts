import { Component ,ViewChild} from '@angular/core';
import { Platform,Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WorldPage } from '../pages/world/world';
import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from "../pages/weather-home/weather-home";
import { SettingPage } from '../pages/setting/setting';

export class MenuItem{
  title : string;
  component?: any;
  name : any;
  icon : string;
  index? : number;
}
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = TabsPage;
  Menu: Array<MenuItem> = [
            {title: 'Home', name: 'TabsPage', component: HomePage, icon: 'home', index: 0},
            {title: 'World', name: 'TabsPage', component: WorldPage, icon: 'globe', index: 1},
        ];;
  Setting: Array<MenuItem> = [
    {title: 'Setting', name: 'SettingPage', icon: 'settings'}
      ];;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
  
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
 
  openPage(page: MenuItem) {
    let params = {};
    if (page.index) {
      params = { tabIndex: page.index };
    }
    if (this.nav.getActiveChildNav() && page.index != undefined) {
      this.nav.getActiveChildNav().select(page.index);
    } else {
      this.nav.setRoot(page.name, params);
    }
  }
 
  isActive(page: MenuItem) {
    let childNav = this.nav.getActiveChildNav();
    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.component) {
        return 'primary';
      }
      return;
    }
    if (this.nav.getActive() && this.nav.getActive().name === page.name) {
      return 'primary';
    }
    return;
  }
}
