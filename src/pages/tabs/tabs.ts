import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { WorldPage} from '../world/world';
import { HomePage } from "../weather-home/weather-home";

/**
 * Generated class for the TabsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  
    tab1Root = HomePage;
    tab2Root = WorldPage;
    myIndex: number;
    
    constructor(public navParams : NavParams) {
      this.myIndex = navParams.data.tabIndex || 0;
    }
  }
  