import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { MetricTemp, MetricLength, MetricDistance, MetricPressure, Metrics, MyLocation, DEFAULT_METRICS } from "../../providers/index";

/**
 * Generated class for the SettingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage({
  name : 'SettingPage',
})
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  public enumTemp = MetricTemp;
  public enumLength = MetricLength;
  public enumDistance = MetricDistance;
  public enumPressure = MetricPressure;
  metrics: Metrics;
  homeLocation: MyLocation;
  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl : ModalController) {
  }
  ngOnInit() {
    this.metrics = JSON.parse(localStorage.getItem("metric")) || DEFAULT_METRICS;
    this.homeLocation  = JSON.parse(localStorage.getItem("firstTime"));

  }

  metricChange() {
    localStorage.setItem("metric",JSON.stringify(this.metrics));
    this.metrics = JSON.parse(localStorage.getItem("metric")) || DEFAULT_METRICS;
  }

  changeHomeLocation() {
    console.log("hello");
    let modal = this.modalCtrl.create('LocationPage', { heading: 'Enter Home City' });
    modal.onDidDismiss((data: MyLocation) => {
      if (data) {
        localStorage.setItem("firstTime",JSON.stringify(data));
        this.homeLocation  = JSON.parse(localStorage.getItem("firstTime"));
      }
    });
    modal.present();
  }
}
