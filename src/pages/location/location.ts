import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { MyLocation } from '../../providers/model/model'
declare let google: any;
import * as _ from 'lodash';
/**
 * Generated class for the LocationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {
  @ViewChild('searchInput') searchInput;
  heading : string;
  showCancel : boolean;
  query : any;
  acService: any;
  autocompleteItems :  Array<{ description: string, place_id: string }>;
  locationObj: MyLocation = new MyLocation();


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,public zone: NgZone) {
    console.log("location",this.navParams.data);
    this.heading = navParams.data.heading;
    this.showCancel = navParams.data.showCancel;
    console.log("MyLocation",this.locationObj);
  }

  ionViewWillEnter() {
  this.acService = new google.maps.places.AutocompleteService();
  this.autocompleteItems = [];
  this.query = '';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationPage');
  }

  dismiss(a){
    this.viewCtrl.dismiss(a);
  }

  updateSearch(){
    console.log("hehe",this.query);
    if(this.query.trim() ==''){
      this.autocompleteItems = [];
      return ;
    }
    let config = {
      types: ['(cities)'],
      input : this.query,
    }
    this.acService.getPlacePredictions(config,( predictions , status ) => {
      console.log("acservice",predictions, status);
      this.zone.run(() => {
        this.autocompleteItems = predictions ? predictions : [];
      })

    })
  }
  
  chooseItem(item){
    console.log("item",this.locationObj);
    let request = {
      placeId : item.place_id
    }
    let res  : MyLocation;
    
    let placesService = new google.maps.places.PlacesService(document.createElement('div'));
    console.log("choose",placesService);
    placesService.getDetails(request , ( place, status ) => {
      if(status == google.maps.places.PlacesServiceStatus.OK){
        this.locationObj.lat = place.geometry.location.lat();
        this.locationObj.lng = place.geometry.location.lng();
        let obj = _.find(place.address_components, ['types[0]', 'locality']);
        console.log("short name",place);
        console.log("short name",obj);
        if (obj) {
          this.locationObj.name = obj['short_name'];
        }
        res = this.locationObj;
        console.log("locationObj",place.geometry);
        console.log("res",res);
        // this.locationObj.
      }
      else{
        console.log("error",status);
      }
      console.log("short_name",res);
      this.dismiss(res);
    })
    
  }
}
