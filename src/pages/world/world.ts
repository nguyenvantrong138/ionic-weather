import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { MyLocation } from '../../providers/model/model';
import { MyList, ForecastService, Forecast, WorldWeather, UtilServiceProvider, Metrics, DEFAULT_METRICS } from "../../providers/index";
import { Subscription } from "rxjs/Subscription";
/**
 * Generated class for the WorldPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-world',
  templateUrl: 'world.html',
})
export class WorldPage {
  arrWorldWeather: Array<WorldWeather>;
  myList : Array<MyList>;
  metrics : Metrics;
  forecastSubscriber : Subscription;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public forecastService : ForecastService,
    public utilService: UtilServiceProvider,public toastCtrl : ToastController ) {
      this.myList = JSON.parse(localStorage.getItem("myList")) || [];
      if(this.myList)
      this.listCheck(this.myList);
  }
  listCheck(data){
    this.metrics = JSON.parse(localStorage.getItem("metric")) || DEFAULT_METRICS;
    this.arrWorldWeather = [];
    if(this.myList)
    for(var i = 0; i < data.length ; i++){
      this.arrWorldWeather.push({
        timezone : data[i].forecast.timezone,
        location : data[i].location,
        firstDailyForecast : data[i].forecast.daily.data[0]
      })
    }
    console.log("thisCheck",this.arrWorldWeather);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad WorldPage');
  }
  addLocation(){
    this.myList = JSON.parse(localStorage.getItem("myList")) || [];
    let modal = this.modalCtrl.create('LocationPage',{ heading: 'Add New City' });
    modal.onDidDismiss(data => {
      this.forecastSubscriber = this.forecastService.getForecast(data)
      .subscribe((data2: Forecast) => {
        for ( var i = 0 ; i < this.myList.length ; i++ )
          {
            if(this.myList[i].location.name == data.name && this.myList[i].location.lat == data.lat && this.myList[i].location.lng == data.lng){
              console.log("name",data.name); 
              this.showToast(data.name + ' already exists');
              return;
            }
          }
        this.myList.push({
          forecast : data2,
          location : data
        })
        localStorage.setItem("myList",JSON.stringify(this.myList));
        this.myList = JSON.parse(localStorage.getItem("myList")) || [];
        this.listCheck(this.myList);
      }, err => {
        console.error(err);
      });
    });
    modal.present();
  }
  delete(location){
    console.log("delete", this.myList);
    for(var i = 0 ; i < this.myList.length ; i++ ){
      if(this.myList[i].location.name == location.name && this.myList[i].location.lat == location.lat && this.myList[i].location.lng == location.lng)
        
        this.myList.splice(i,1);
    }
    console.log("delete1", this.myList);
    localStorage.setItem("myList",JSON.stringify(this.myList));
    this.listCheck(this.myList);
  }
  showToast(message, duration?) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration || 3000
    });
    toast.present();
  }
  locationClicked(location){
    console.log("location",location);
    this.navCtrl.push('CityWeatherPage',{
      location : location
    });
  }
}
