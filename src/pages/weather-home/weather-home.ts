import { Component, ViewChild } from '@angular/core';
import { NavController ,Nav} from 'ionic-angular';
import { Http,Response,RequestOptions,Headers } from '@angular/http';
import { Observable} from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { IonicPage, ModalController } from 'ionic-angular';
import { LocationPage } from '../location/location';
import { MyLocation } from '../../providers';
import { ListWeatherComponent } from'../../components/list-weather/list-weather'
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/map';
@IonicPage()
@Component({
  selector: 'page-weather-home',
  templateUrl: 'weather-home.html'
})
export class HomePage {
  firstTime : MyLocation;
  location : MyLocation;
  constructor(public navCtrl: NavController,private httpService: Http,public storage : Storage,public modelCtrl : ModalController ) {
    this.location = JSON.parse(localStorage.getItem("firstTime"));
  }
  ionViewWillEnter(){
    // this.storage.get("firstTime").then(data => console.log("next",data));
    if(!this.location){
      let profileModel = this.modelCtrl.create('LocationPage',{ heading: 'Enter Home City', showCancel: false });
    profileModel.onDidDismiss((data1,data2) => {
      this.location = data1;
      console.log("location",this.location);
      console.log("home-loc",data1);
      localStorage.setItem("firstTime",JSON.stringify(this.location));
      this.location = JSON.parse(localStorage.getItem("firstTime"));
    })
    profileModel.present();
    }
  }
}
