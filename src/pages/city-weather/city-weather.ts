import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyLocation } from "../../providers/index";

/**
 * Generated class for the CityWeatherPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-city-weather',
  templateUrl: 'city-weather.html',
})
export class CityWeatherPage {
  location : MyLocation;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.location = navParams.data.location;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CityWeatherPage');
  }

}
